


def checkSavingsRate(portionSaved, annualSalary):
    currentSavings = 0
    semiAnnualRaise = 0.07
    interestRate = 0.04
    downPayment = 0.25
    housePrice = 1000000
    numMonths = 0
    while currentSavings < downPayment*housePrice :
        tmp = currentSavings
        monthlySalary = annualSalary/12
        currentSavings = tmp + (tmp*interestRate)/12 + (monthlySalary*portionSaved)
        numMonths = numMonths + 1
        if numMonths % 6 == 0:
            annualSalary = annualSalary*(semiAnnualRaise+1)
        else:
            pass
    return numMonths

initialSavingRate = 5000
upperBound = 10000
lowerBound = 0
numOfBisects = 0
annualSalary = int(input('What is your annual salary: '))


for bisectCount in range(20):
    if checkSavingsRate(initialSavingRate/10000, annualSalary) > 36:
        lowerBound = initialSavingRate
        initialSavingRate = int((upperBound + lowerBound)/2)
        bisectCount += 1
        # print(bisectCount)
    elif checkSavingsRate(initialSavingRate/10000, annualSalary) < 36:
        upperBound = initialSavingRate
        initialSavingRate = int((upperBound + lowerBound)/2)
        bisectCount += 1
        # print(bisectCount)
    elif checkSavingsRate(initialSavingRate/10000, annualSalary) == 36:
        bisectCount += 1
        # print(bisectCount)
        print('Best Savings Rate is', initialSavingRate/10000)
        break
