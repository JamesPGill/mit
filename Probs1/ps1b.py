annualSalary = int(input('What is your annual salary: '))
semiAnnualRaise = float(input('What percentage of your salary is yout semi annual raise: '))/100
portionSaved = float(input('What percentage of salary would you like to save: '))/100
housePrice = int(input('What is the cost of your desired house: '))


portionDownPayment = 0.25
interestRate = 0.04
currentSavings = 0

numMonths = 0

while currentSavings < portionDownPayment*housePrice :
    tmp = currentSavings
    monthlySalary = annualSalary/12
    currentSavings = tmp + (tmp*interestRate)/12 + (monthlySalary*portionSaved)
    numMonths = numMonths + 1
    if numMonths % 6 == 0:
        annualSalary = annualSalary*(semiAnnualRaise+1)


print('It will take', numMonths, 'months to save the down payment.')
