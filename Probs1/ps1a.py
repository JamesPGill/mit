annualSalary = int(input('What is your annual salary: '))
portionSaved = float(input('What percentage of salary would you like to save: '))/100
housePrice = int(input('What is the cost of your desired house: '))

portionDownPayment = 0.25
interestRate = 0.04
currentSavings = 0
monthlySalary = annualSalary/12
numMonths = 0

while currentSavings < portionDownPayment*housePrice :
    tmp = currentSavings
    currentSavings = tmp + (tmp*interestRate)/12 + (monthlySalary*portionSaved)
    numMonths = numMonths + 1

print('It will take', numMonths, 'months to save the down payment.')
