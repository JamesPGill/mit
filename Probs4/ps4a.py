# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx


def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''

    if len(sequence) == 0:
        return ['']
    prev_list = get_permutations(sequence[1:len(sequence)])
    print("prev list =>", prev_list)
    next_list = []
    for i in range(0, len(prev_list)):
        for j in range(0, len(sequence)):
            new_sequence = prev_list[i][0:j] + \
                sequence[0] + prev_list[i][j:len(sequence) - 1]
            if new_sequence not in next_list:
                next_list.append(new_sequence)
    next_list.sort()
    print('next list =>', next_list)
    return next_list


if __name__ == '__main__':
    # EXAMPLE
    example_input = 'bust'
    print('Input:', example_input)
    # print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
    print('Actual Output:', get_permutations(example_input))

    # Put three example test cases here (for your sanity, limit your inputs
    # to be three characters or fewer as you will have n! permutations for a
    # sequence of length n)
    # print('\n')
    # ExampleTwo = 'yellow'
    # print('Input:', ExampleTwo)
    # print('Actual Output:', get_permutations(ExampleTwo))
