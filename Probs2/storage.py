while numGuesses > 0:
     print('You have', numGuesses, 'guesses remaining.')
     availableLetters = get_available_letters(letters_guessed)
     print('Available Letters:', availableLetters)
     guess = input('Guess a Letter:')
     guess = guess.lower()
     if guess not in string.ascii_lowercase:
         if numWarnings > 0:
             numWarnings = numWarnings - 1
             print('You have lost a warning. Please enter a valid letter.')
         elif numWarnings == 0:
             print('You have lost a guess for using up your warnings. Please enter a valid letter.')
         elif guess in availableLetters:
             if guess in secret_word:
                 currentWord = get_guessed_word(secret_word, letters_guessed)
                 print('Good Guess:', currentWord)
                 if currentWord == secret_word:
                     print('You win.')
             else:
                 print('Sorry that is not in the word.')
                 numGuesses -= 1
                 if numGuesses == 0: print('You Lose.')
